(function () {
  var login = angular.module( 'login-module', [] );
  login.controller( 'LoginController', ['$scope', '$q', '$timeout', function ( $scope, $q, $timeout ) {
    $scope.user = {};
    $scope.errorMessage = '';
    $scope.login = function ( user ) {
      emulateFailUserCreation( user ).then( function () {
        console.log( 'The user ' + user.username + ' has been created.' );
      }, function ( error ) {
        $scope.errorMessage = error.message[0];
        console.log( $scope.errorMessage );
      } );
    };
    function emulateFailUserCreation() {
      return $q.reject( {message: ['This name has been taken.']} );
    }
  }] );
  /**
   * El servicio setFormErrors permite a la directiva withErrors registrarse a si mismas
   * El servicio configurará los errores de los campos para un formulario dado.
   */
  login.factory( 'setFormErrors', function () {
    var withErrorControllers = {};
    var setFormErrorsService = function ( opts ) {
      var fieldErrors = opts.fieldErrors;
      var controller = withErrorControllers[opts.formName];
      Object.keys( fieldErrors ).forEach( function ( fieldName ) {
        controller.setErrorsFor( fieldName, fieldErrors[fieldName] );
      } );
    };
    setFormErrorsService._register = function (formName, controller) {
      withErrorControllers[formName] = controller;
    };
    return setFormErrorsService;
  } );
  /**
   * El controlador de la directiva withErrors permite a los controladores de la directiva
   * fielderrors registrarse a si mismas.
   * También se registra a si misma con el servicio setFormErrors de modo que se puedan
   * configurar errores adicionales sin validaciones Angular.
   * Por último provee dos métodos para la directiva input que podrá ejecutar en cualquier momento
   * en el que se encuentre con un error de validación de Angular.
   */
  login.directive( 'withErrors', ['setFormErrors', function ( setFormErrors ) {
    function isFieldNameInControls(fieldName, controls){
      return fieldName in controls;
    }
    return {
      restrict: 'A',
      require: 'withErrors',
      controller: ['$scope', '$element', function ($scope, $element) {
        var controls = {};
        this.addControl = function (fieldName, controller) {
          controls[fieldName] = controller;
        };
        this.setErrorsFor = function (fieldName, errors) {
          if(!(isFieldNameInControls(fieldName, controls))) {
            return;
          }
          return controls[fieldName].setErrors(errors);
        };
        this.clearErrorsFor = function (fieldName, errors) {
          if(!(isFieldNameInControls(fieldName, controls))) {
            return;
          }
          return controls[fieldName].clearErrors(errors);
        };
      }],
      link: function (scope, element, attrs, controller) {
        setFormErrors._register(attrs.name, controller);
      }
    };
  }] );
  /**
   * La directiva input requiere dos controladores de las directivas ngModel y withErrors.
   * Si los dos estan presentes entonces escucharemos cualquier error en ngModel, los mapearemos
   * a mensajes y los configuraremos usando el controlador de withErrors
   */
  login.directive( 'input', function () {

  } );
  /**
   * La directiva fielderrors requiere del controlador de una directiva padre withErros la cual
   * se registrará a si misma. Y también ha de proveer métodos para configurar y eliminar errores.
   */
  login.directive( 'fielderrors', function () {

  } );
}());