(function () {
  var login = angular.module( 'login-module', [] );
  login.controller( 'LoginController', ['$scope', function ( $scope ) {
    $scope.user = {};
  }] );
}());