(function () {
  var login = angular.module( 'login-module', [] );
  login.controller( 'LoginController', ['$scope', '$q', function ( $scope, $q ) {
    $scope.user = {};
    $scope.errorMessage = '';
    $scope.login = function ( user ) {
      emulateFailUserCreation( user ).then( function () {
        console.log( arguments );
        console.log( 'The user ' + user.username + ' has been created.' );
      }, function ( error ) {
        $scope.errorMessage = error.message[0];
        console.log( $scope.errorMessage );
      } );
    };
    function emulateFailUserCreation() {
      return $q.reject( {message: ['This name has been taken.']} );
    }
  }] );
}());