(function () {
  var login = angular.module( 'login-module', [] );
  login.controller( 'LoginController', ['$scope', '$q', '$timeout', function ( $scope, $q, $timeout ) {
    $scope.user = {};
    $scope.errorMessage = '';
    $scope.login = function ( user ) {
      emulateFailUserCreation( user ).then( function () {
        console.log( 'The user ' + user.username + ' has been created.' );
      }, function ( error ) {
        $scope.errorMessage = error.message[0];
        console.log( $scope.errorMessage );
      } );
    };
    function emulateFailUserCreation() {
      return $q.reject( {message: ['This name has been taken.']} );
    }

    function emulateSuccessUserCreation() {
      var deferred = $q.defer();
      $timeout( function () {
        deferred.resolve( {error: false} );
      }, 18 );
      return deferred.promise;
    }
  }] );
  /**
   * El servicio setFormErrors permite a la directiva withErrors registrarse a si mismas
   * El servicio configurará los errores de los campos para un formulario dado.
   */
  login.factory( 'setFormErrors', function () {
  } );
  /**
   * El controlador de la directiva withErrors permite a los controladores de la directiva
   * fielderrors registrarse a si mismas.
   * También se registra a si misma con el servicio setFormErrors de modo que se puedan
   * configurar errores adicionales sin validaciones Angular.
   * Por último provee dos métodos para la directiva input que podrá ejecutar en cualquier momento
   * en el que se encuentre con un error de validación de Angular.
   */
  login.directive( 'withErrors', ['setFormErrors', function ( setFormErrors ) {

  }] );
  /**
   * La directiva input requiere dos controladores de las directivas ngModel y withErrors.
   * Si los dos estan presentes entonces escucharemos cualquier error en ngModel, los mapearemos
   * a mensajes y los configuraremos usando el controlador de withErrors
   */
  login.directive( 'input', function () {

  } );
  /**
   * La directiva fielderrors requiere del controlador de una directiva padre withErros la cual
   * se registrará a si misma. Y también ha de proveer métodos para configurar y eliminar errores.
   */
  login.directive( 'fielderrors', function () {

  } );
}());